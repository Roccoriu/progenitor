const std = @import("std");
const progenitor = @import("./lib/progenitor.zig");

const debug = std.debug;

fn printHelp(val: ?[]const u8) void {
    debug.print("helping: {s}\n", .{val orelse ""});
}

const commands = [_]progenitor.Command{
    .{
        .name = "install",
        .help = "this is the help function",
        .cmd = printHelp,
        .flags = &[_]progenitor.Flag{
            .{
                .short = 'c',
                .long = "config",
                .takesValue = true,
                .help = "sets the config",
            },
        },
        .subCmd = &[_]progenitor.Command{
            .{
                .name = "pkg",
                .help = "this is the help function",
                .cmd = printHelp,
                .subCmd = &[_]progenitor.Command{
                    .{
                        .name = "add",
                        .help = "this is the help function",
                        .cmd = printHelp,
                    },
                },
            },
        },
    },

    .{
        .name = "uninstall",
        .help = "this is the help function",
        .cmd = printHelp,
        .flags = &[_]progenitor.Flag{
            .{
                .short = 'c',
                .long = "config",
                .takesValue = true,
                .help = "sets the config",
            },
        },
        .subCmd = &[_]progenitor.Command{
            .{
                .name = "name",
                .help = "this is the help function",
                .cmd = printHelp,
                .subCmd = &[_]progenitor.Command{
                    .{
                        .name = "rm",
                        .help = "this is the help function",
                        .cmd = printHelp,
                    },
                },
            },
        },
    },
};

pub fn main() !void {
    const alloc = std.heap.page_allocator;

    var cli = try progenitor.Cli.init(&commands, alloc);
    defer cli.deinit();

    try cli.run();
}
