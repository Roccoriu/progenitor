const std = @import("std");
const ArgPeekIterator = @import("./iter.zig").ArgPeekIterator;

pub const Flag = @import("./flag.zig").Flag;
pub const Command = @import("./command.zig").Command;
pub const ArgumentType = @import("./arg_type.zig").ArgumentType;

const debug = std.debug;
const io = std.io;
const mem = std.mem;
const process = std.process;

pub const Error = error{
    InvalidCommand,
    DuplicateFlag,
};

pub const Cli = struct {
    argIter: ArgPeekIterator,
    cmdStack: std.ArrayList([]const Command),

    currentCmd: ?Command = null,

    values: std.StringHashMap([]const u8),

    pub fn run(self: *Cli) !void {
        while (self.argIter.next()) |arg| {
            try self.handleNs(arg);
        }
    }

    // TODO: figure out way to handle duplicate flags
    fn handleNs(self: *Cli, arg: ?[]const u8) !void {
        const cmds = if (self.cmdStack.items.len > 0) self.cmdStack.pop() else null;
        const argType = ArgumentType.init(arg, cmds);

        switch (argType) {
            .command => {
                const current = getCommand(arg, cmds) orelse return;
                const nextArgType = ArgumentType.init(self.argIter.peek(), current.subCmd);

                self.currentCmd = current;

                if (current.subCmd != null and nextArgType == .command) {
                    try self.cmdStack.append(current.subCmd.?);
                    return;
                }

                if (current.cmd) |func| func(current.name);
            },

            .longFlag, .shortFlag => {
                if (self.currentCmd == null) return;

                if (getFlag(arg, self.currentCmd.?.flags, argType)) |flag| {
                    if (self.values.get(flag.long)) |_| {
                        debug.print("flag: {s}\n was already called", .{flag.long});
                        return;
                    }

                    try self.values.put(flag.long, "something");

                    debug.print("flag: {s}\n", .{flag.long});
                }
            },

            // TODO: add support for positional values passed to command
            .value => debug.print("value: {s}\n", .{arg orelse ""}),

            else => debug.print("something is wrong", .{}),
        }
    }

    fn getFlag(arg: ?[]const u8, flags: ?[]const Flag, argType: ArgumentType) ?Flag {
        if (arg == null or flags == null) return null;

        for (flags.?) |flag| {
            if (flag.isMatch(arg.?, argType)) return flag;
        }

        return null;
    }

    fn getCommand(arg: ?[]const u8, cmds: ?[]const Command) ?Command {
        if (arg == null or cmds == null) return null;

        for (cmds.?) |cmd| {
            if (cmd.isMatch(arg.?)) return cmd;
        }

        return null;
    }

    pub fn deinit(self: *Cli) void {
        self.argIter.deinit();
        self.cmdStack.deinit();
    }

    pub fn init(cli: []const Command, alloc: std.mem.Allocator) !Cli {
        // TODO: remove reused allocator, this is just for the time being
        var arr = std.ArrayList([]const Command).init(alloc);
        try arr.append(cli);

        return .{
            .argIter = try ArgPeekIterator.init(alloc),

            .cmdStack = arr,
            .values = std.StringHashMap([]const u8).init(alloc),
        };
    }
};
