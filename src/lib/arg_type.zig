const std = @import("std");

const Command = @import("./command.zig").Command;
const Flag = @import("./flag.zig").Flag;

const mem = std.mem;
const proc = std.process;

pub const ArgumentType = enum {
    command,
    longFlag,
    shortFlag,
    value,
    none,

    pub fn init(arg: ?[]const u8, cmds: ?[]const Command) ArgumentType {
        if (isShortFlag(arg))
            return .shortFlag;

        if (isLongFlag(arg))
            return .longFlag;

        if (isCommand(arg, cmds))
            return .command;

        return .value;
    }

    fn isLongFlag(arg: ?[]const u8) bool {
        if (arg == null or arg.?.len < 3) return false;

        return mem.startsWith(u8, arg.?, "--");
    }

    fn isShortFlag(arg: ?[]const u8) bool {
        if (arg == null or arg.?.len != 2) return false;

        return mem.startsWith(u8, arg.?, "-");
    }

    fn isCommand(arg: ?[]const u8, cmds: ?[]const Command) bool {
        if (arg == null or cmds == null) return false;

        for (cmds.?) |cmd| {
            if (cmd.isMatch(arg.?)) return true;
        }

        return false;
    }
};
