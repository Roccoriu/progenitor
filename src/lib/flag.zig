const std = @import("std");
const ArgumentType = @import("./arg_type.zig").ArgumentType;

const mem = std.mem;

pub const Flag = struct {
    short: u8,
    long: []const u8,

    //check for better way to handle this
    takesValue: bool = false,
    help: []const u8,

    pub fn isMatch(self: Flag, arg: []const u8, argType: ArgumentType) bool {
        if (arg.len < 2) return false;

        if (argType == .longFlag and mem.eql(u8, arg[2..], self.long))
            return true;

        if (argType == .shortFlag and arg[1] == self.short and arg.len == 2)
            return true;

        return false;
    }
};
