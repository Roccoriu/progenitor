const std = @import("std");

const mem = std.mem;
const proc = std.process;

pub const ArgPeekIterator = struct {
    _current: ?[]const u8 = null,
    _next: ?[]const u8 = null,

    iter: proc.ArgIterator,

    pub fn init(alloc: mem.Allocator) !ArgPeekIterator {
        var iter = try std.process.ArgIterator.initWithAllocator(alloc);
        _ = iter.next();

        return ArgPeekIterator{ .iter = iter };
    }

    pub fn peek(self: ArgPeekIterator) ?[]const u8 {
        return self._next;
    }

    pub fn skip(self: *ArgPeekIterator) bool {
        return self.iter.skip();
    }

    pub fn next(self: *ArgPeekIterator) ?[]const u8 {
        if (self._current == null and self._next == null) {
            self._current = self.iter.next();
            self._next = self.iter.next();
            return self._current;
        }

        self._current = self._next;
        self._next = self.iter.next();

        return self._current;
    }

    pub fn deinit(self: *ArgPeekIterator) void {
        self.iter.deinit();
    }
};
