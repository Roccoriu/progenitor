const std = @import("std");
const Flag = @import("./flag.zig").Flag;

const mem = std.mem;

pub const Command = struct {
    name: []const u8,
    help: []const u8,
    called: bool = false,
    cmd: ?*const fn (val: ?[]const u8) void = null,

    flags: ?[]const Flag = null,
    subCmd: ?[]const Command = null,

    pub fn isMatch(self: Command, arg: []const u8) bool {
        if (mem.eql(u8, self.name, arg)) return true;

        return false;
    }
};
